var assert = require('assert');
let input=require('./elements');
let inputD=require('./testdata');
var {Then} = require('cucumber');
const expect = require('chai').expect;
const {Builder, By, until,sendKeys,timeout,isExisting} = require('selenium-webdriver');


module.exports = function () {

    this.Given(/^I visit Dominos$/, function() {
            return this.driver.get(input.url);
    });
    this.Then(/^click on Our Menu$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.ourMenu)), 30000, 'Looking for element').click();});
    this.Then(/^click on Veg Pizzas$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.vegPizzas)), 30000, 'Looking for element').click();});
    this.Then(/^click on Veg View All$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.vegViewAll)), 30000, 'Looking for element').click();});
    this.Then(/^select Peppy Paneer and click on order now$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.peppyPaneer)), 30000, 'Looking for element').click();});
    this.Then(/^click on Locate Me$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.locateMe)), 30000, 'Looking for element').click();});
    this.Then(/^select location from list$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.locationlist)), 30000, 'Looking for element').click();});
    this.Then(/^click on the city$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.selectcity)), 30000, 'Looking for element').click();});
    this.Given(/^fill the city$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.selectcity)), 30000, 'Looking for element').sendKeys(inputD.city);});
    this.Then(/^click on vishakhapatnam$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.clickvizag)), 30000, 'Looking for element').click();});
    this.Then(/^click on the locality$/, async function() {
        await new Promise(resolve => setTimeout(resolve, 3000));
        return this.driver.wait(until.elementLocated(By.xpath(input.selectlocality)), 30000, 'Looking for element').click();});
    this.Given(/^fill the locality$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.selectlocality)), 30000, 'Looking for element').sendKeys(inputD.locality);});
    this.Then(/^click on beach road$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.clickbeach)), 30000, 'Looking for element').click();});
    this.Then(/^click on proceed$/,  function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.proceed)), 30000, 'Looking for element').click();});
    this.Then(/^click on customized pizza$/, function() {
        return this.driver.wait (until.elementLocated(By.xpath(input.customise)), 30000, 'Looking for element').click();});
    this.Then(/^click on Add extra cheese$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.extracheese)), 30000, 'Looking for element').click();});
    this.Then(/^click on Add toppings$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.AddTop)), 30000, 'Looking for element').click();});
    this.Then(/^click on Add to Cart$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.addtocart)), 30000, 'Looking for element').click();});
    this.Then(/^click on Checkout$/, function() {
        return this.driver.wait(until.elementLocated(By.xpath(input.checkout)), 30000, 'Looking for element').click();});


};


